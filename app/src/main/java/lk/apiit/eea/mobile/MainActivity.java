package lk.apiit.eea.mobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import java.util.List;

import lk.apiit.eea.mobile.Activities.SignIn;
import lk.apiit.eea.mobile.Adapters.CustomSharedPreferences;

public class MainActivity extends AppCompatActivity {
    private CustomSharedPreferences preferences;

    private static int SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                        startActivity( new Intent(MainActivity.this, SignIn.class));
                        finish();
            }
        },SPLASH_TIME_OUT
        );

//        boolean loginStatus = preferences.getABool(getApplicationContext(),"loginstatus");

//        if(loginStatus){
//            startActivity( new Intent(MainActivity.this, SignIn.class));
//            finish();
//        }


    }

}
