package lk.apiit.eea.mobile.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import lk.apiit.eea.mobile.R;
import lk.apiit.eea.mobile.fragments.RegisterFragment;
import lk.apiit.eea.mobile.fragments.SignInFragment;

public class SignIn extends AppCompatActivity implements SignInFragment.OnFragmentInteractionListener{

    public static FragmentManager fragmentManager;
    private Bundle state;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in);

        fragmentManager = getSupportFragmentManager();

        if(findViewById(R.id.BaseLayout)!=null){
            if(savedInstanceState!=null){
                return;
            }


            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

//            RegisterFragment  registerFragment = new RegisterFragment();
            SignInFragment signInFragment = new SignInFragment();

            fragmentTransaction.add(R.id.BaseLayout, signInFragment);

            fragmentTransaction.commit();
        }
    }

    private void changeFragment(){
        Bundle savedInstanceState = state;
        fragmentManager = getSupportFragmentManager();
                if(findViewById(R.id.Register_Fragment)!=null){
                    if(savedInstanceState!=null){
                        return;
                    }


                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    RegisterFragment  registerFragment = new RegisterFragment();

                    fragmentTransaction.add(R.id.Register_Fragment, registerFragment);

                    fragmentTransaction.commit();
                }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


//    @Override
//    public void onClick(View view) {
//        switch(view.getId()){
//            case R.id.goToRegisterBtn:
//                changeFragment(state);
//                break;
//        }
//    }
}
